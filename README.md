# AngularScope

## Français 

Une extension Firefox pour inspecter le scope Angular lié à un noeud du DOM.

* téléchargement de l'extension : https://addons.mozilla.org/fr/firefox/addon/angularscope/
* licence: [WTFPL](https://framagit.org/Darathor/angular-scope/raw/master/LICENSE)

Captures d'écran :

![Menu contextuel](https://framagit.org/Darathor/angular-scope/raw/master/resources/screenshot01.png)

## English 

A Firefox extension to inspect AngularJS scope that a DOM element binds to.

* extension download: https://addons.mozilla.org/fr/firefox/addon/angularscope/
* license: [WTFPL](https://framagit.org/Darathor/angular-scope/raw/master/LICENSE)

Screenshots:

![Context menu](https://framagit.org/Darathor/angular-scope/raw/master/resources/screenshot01.png)