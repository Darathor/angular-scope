/**
 * @author       Darathor < darathor@free.fr >
 * @license      WTFPL
 *
 * Code inspired on https://github.com/christophehurpeau/ng-inspect
 */
(function(window, browser) {
	browser.contextMenus.create({
		title: browser.i18n.getMessage('angularScopeContextMenuItemLabel'),
		contexts: ['all'],
		onclick: function(info, tab) {
			browser.tabs.sendMessage(tab.id, { action: 'log-scope' });
		}
	});
})(window, window.browser);