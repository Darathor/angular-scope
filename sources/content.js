/**
 * @author       Darathor < darathor@free.fr >
 * @license      WTFPL
 *
 * Code inspired on https://github.com/christophehurpeau/ng-inspect
 */
(function(window, browser) {
	let script = document.createElement('script');
	script.setAttribute('type', 'application/javascript');
	script.textContent = `document.addEventListener('mousedown', function(event) {
		if (event.button === 2) {
			window._angularScope_clickedElement = event.target;
		}
	}, true);`;
	document.documentElement.appendChild(script);
	document.documentElement.removeChild(script);

	browser.runtime.onMessage.addListener(function(request) {
		if (request.action === 'log-scope') {
			let script = document.createElement('script');
			script.setAttribute('type', 'application/javascript');
			script.textContent = `if (!window.angular) {
				alert("${browser.i18n.getMessage('angularScopeErrorNoAngularFound')}");
			}
			else if (window._angularScope_clickedElement) {
				var scope = angular.element(window._angularScope_clickedElement).scope();
				if (!scope) {
					if (confirm("${browser.i18n.getMessage('angularScopeConfirmReloadWithDebug')}")) {
						angular.reloadWithDebugInfo();
					}
				}
				else {
					var cleanScope = {};
					var prototype = scope;
					do {
						angular.forEach(prototype, function(value, key) {
							if (!(key in cleanScope)) {
								cleanScope[key] = value;
							}
						});
					}
					while (prototype = prototype.__proto__);
					console.group('Angular scope');
					console.log('Brut scope:', scope);
					console.log('Available variables:', cleanScope);
					console.log('Element:', window._angularScope_clickedElement);
					console.groupEnd();
				}
			}`;
			document.documentElement.appendChild(script);
			document.documentElement.removeChild(script);
		}
	});
})(window, browser);